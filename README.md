# Example of Pool that can spawn prefabs by enum parameter for Unity + Zenject
This is simple example how I use Zenject and how to use my PoolEnumToPrefab. 

Unity version 2017.1.0f3  
Zenject version 5.2.1

##Gameplay flow
After a player used a mouse to click **InputController** raytraced from MainCamera, if the ray hit **PlaneActor** instance then **ClickOnPlaneSignal** fired.
Because **GameManager** subscribed to **ClickOnPlaneSignal**, when it fired **GameManager** spawned **CharacterActor** based on **PlaneActor.CharacterType** 
with a help of the **CharacterSpawner**.   

![Scheme](ImageForReadme.jpg)

##Check this files
* **MainInstaller** for all Zenject bindings 
* **PoolEnumToPrefab** my abstract pool
* **GameManager** and **InputController** for gameplay code
* Files in Character folder for implementation of PoolEnumToPrefab

##For more information about Zenject
* [Zenject repository](https://github.com/modesttree/Zenject)
* I've posted the code of PoolEnumToPrefab first time in this [Zenject Google User Group topic](https://groups.google.com/forum/#!topic/zenject/4LqbdssSIYs)


