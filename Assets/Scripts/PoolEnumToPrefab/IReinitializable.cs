﻿/// <summary>
/// Reinitialize called by pool right after spawn
/// It's method in which you should place the code that prepares instance for use
/// </summary>
public interface IReinitializable
{
    void Reinitialize();
}
