﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
/// <summary>
/// Pool which can create different prefabs by enum parameter value
/// </summary>
public abstract class PoolEnumToPrefab<TParam1, TValue> where TValue : Component, IReinitializable
{
    Dictionary<TParam1, CustomMonoMemoryPool<TValue>> _pools = new Dictionary<TParam1, CustomMonoMemoryPool<TValue>>();
    DiContainer _container;
    MemoryPoolSettings _settings;
    IFactory<TParam1, TValue> _factory;


    public PoolEnumToPrefab(DiContainer container, MemoryPoolSettings settings, IFactory<TParam1, TValue> factory)
    {
        _container = container;
        _settings = settings;
        _factory = factory;
        RegisterAllPools();
    }
    public TValue Spawn(TParam1 param)
    {
        return _pools[param].Spawn();
    }
    public void Despawn(TParam1 param, TValue gameObject)
    {
        _pools[param].Despawn(gameObject);
    }
    /// <summary>
    /// Creating pools for all enum parameter values, you must provide prefabs for all enum parameter values
    /// if you don't provide prefab corresponding to an enum value and will try to create there will be an error
    /// </summary>
    void RegisterAllPools()
    {
        var enumValues = Enum.GetValues(typeof(TParam1));
        for (int i = 0; i < enumValues.Length; i++)
        {
            var nEnum = (TParam1)enumValues.GetValue(i);
            var nPool = CreatePool(nEnum);
            _pools.Add(nEnum, nPool);
        }
    }
    CustomMonoMemoryPool<TValue> CreatePool(TParam1 param)
    {
        return _container.Instantiate<CustomMonoMemoryPool<TValue>>(new object[] { _settings, new FactoryProxy<TValue>(param, _factory) });
    }
    /// <summary>
    /// Factory which store enum parameter and redirect creation to _factory  
    /// Needed cause when we call CreatePool we have to provide as a parameter a Factory that don't take any parameters 
    /// </summary>
    class FactoryProxy<TValue> : IFactory<TValue>
    {
        TParam1 _param;
        IFactory<TParam1, TValue> _factory;
        public FactoryProxy(TParam1 param, IFactory<TParam1, TValue> factory)
        {
            _param = param;
            _factory = factory;
        }
        public TValue Create()
        {
            return _factory.Create(_param);
        }
    }
    class CustomMonoMemoryPool<TValue> : MonoMemoryPool<TValue> where TValue : Component, IReinitializable
    {
        protected override void OnCreated(TValue item)
        {
            base.OnCreated(item);
        }
        protected override void OnSpawned(TValue item)
        {
            base.OnSpawned(item);
        }
        protected override void OnDespawned(TValue item)
        {
            base.OnDespawned(item);
        }
        /// <summary>
        /// Called right after spawning from pool
        /// </summary>
        /// <param name="item"></param>
        protected override void Reinitialize(TValue item)
        {
            item.Reinitialize();
        }
    }
}