﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
/// <summary>
/// Factory that creates CharacterActor by CharacterType
/// </summary>
public class CharacterFactory : IFactory<CharacterType, CharacterActor>
{
    DiContainer _container;
    Settings _settings;


    public CharacterFactory(DiContainer container, Settings settings)
    {
        _container = container;
        _settings = settings;
    }
    public CharacterActor Create(CharacterType param)
    {
        return _container.InstantiatePrefabForComponent<CharacterActor>(GetPrefab(param));
    }
    GameObject GetPrefab(CharacterType param)
    {
        return _settings.Prefabs[(int)param].gameObject;
    }
    [Serializable]
    public class Settings
    {
        public List<CharacterActor> Prefabs;
    }
}
