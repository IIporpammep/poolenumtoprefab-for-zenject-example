﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
/// <summary>
/// Implementation of PoolEnumToPrefab for CharacterActor
/// Don't use it directly in CharacterActor cause Zenject will find circular dependency
/// </summary>
public class CharacterPool : PoolEnumToPrefab<CharacterType, CharacterActor>
{
    [Inject]
    public CharacterPool(DiContainer container, MemoryPoolSettings settings, CharacterActor.Factory factory) : base(container, settings, factory) { }
}
