﻿using System.Collections;
using UnityEngine;
using Zenject;
/// <summary>
/// After CharacterActor spawn it starts to scale down coroutine, when timer runs out instance despawn itself
/// </summary>
public class CharacterActor : MonoBehaviour, IReinitializable
{
    [SerializeField]
    CharacterType _type;
    CharacterSpawner _spawner;
    float _scaleDownTime;

    /// <summary>
    /// Injection method Zenject calls it and tries to resolve dependencies based on bindings in Maininstaller
    /// [Inject(Id = "ScaleDownTime")] it's identificator(string = "ScaleDownTime") injection
    /// </summary>
    [Inject]
    void Construct(CharacterSpawner spawner, [Inject(Id = "ScaleDownTime")]  float scaleDownTime)
    {
        _spawner = spawner;
        _scaleDownTime = scaleDownTime;
    }
    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }

    public CharacterType GetCharacterType()
    {
        return _type;
    }
    /// <summary>
    /// Call right after spawning
    /// </summary>
    public void Reinitialize()
    {
        StartCoroutine(ScalingDown());
    }
    IEnumerator ScalingDown()
    {
        float timer = _scaleDownTime;
        while (timer > 0)
        {
            gameObject.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, 1 - timer / _scaleDownTime);
            timer -= Time.deltaTime;
            yield return null;
        }
        _spawner.Despawn(this);
    }

    public class Factory : Factory<CharacterType, CharacterActor> { }
}
