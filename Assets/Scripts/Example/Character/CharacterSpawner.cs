﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
/// <summary>
/// Used to spawn and despawn CharacterActors based on CharacterType value 
/// </summary>
public class CharacterSpawner : MonoBehaviour
{
    CharacterPool _pool;


    [Inject]
    void Construct(CharacterPool characterPool)
    {
        _pool = characterPool;
    }
    public CharacterActor Spawn(CharacterType type)
    {
        return _pool.Spawn(type);
    }
    public void Despawn(CharacterActor character)
    {
        _pool.Despawn(character.GetCharacterType(), character);
    }
}
