﻿using Zenject;
/// <summary>
/// Store all game settings
/// </summary>
[System.Serializable]
public class GameConfig
{
    public float ScaleDownTime;
    public MemoryPoolSettings DefaultMemoryPoolSettings;
    public CharacterFactory.Settings CharacterFactorySettings;
}
