﻿using UnityEngine;
using Zenject;
/// <summary>
/// Simple game example manager 
/// When a player clicks(ClickOnPlaneSignal fired) on a plane GameManager spawn a CharacterActor based on the plane characterType
/// </summary>
public class GameManager : MonoBehaviour
{
    ClickOnPlaneSignal _clickOnPlaneSignal;
    CharacterSpawner _characterSpawner;


    [Inject]
    void Construct(CharacterSpawner characterSpawner, ClickOnPlaneSignal clickOnPlaneSignal)
    {
        _characterSpawner = characterSpawner;
        _clickOnPlaneSignal = clickOnPlaneSignal;
        _clickOnPlaneSignal += OnClickOnPlane;
    }
    void OnDestroy()
    {
        _clickOnPlaneSignal -= OnClickOnPlane;
    }
    void OnClickOnPlane(Vector3 clickPosition, CharacterType characterType)
    {
        var newCharacter = _characterSpawner.Spawn(characterType);
        newCharacter.SetPosition(clickPosition);
    }
}
