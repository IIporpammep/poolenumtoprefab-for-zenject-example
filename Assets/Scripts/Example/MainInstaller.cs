using UnityEngine;
using Zenject;
/// <summary>
/// Installs all dependencies of the Main scene
/// </summary>
public class MainInstaller : MonoInstaller<MainInstaller>
{
    [SerializeField]
    GameConfig _gameConfig;


    public override void InstallBindings()
    {
        //FromInstance - in all MemoryPoolSettings injects _gameConfig.DefaultMemoryPoolSettings
        //AsSingle - don't create new instances when inject 
        Container.Bind<MemoryPoolSettings>().FromInstance(_gameConfig.DefaultMemoryPoolSettings).AsSingle();

        BindCharacters();

        //For the sake of example we use identificator binding, the better approach is to create CommonCharacterCharacteristics with ScaleDownTime float, and bind from instance
        //especially if we need to more than one common characteristics
        Container.Bind<float>().WithId("ScaleDownTime").FromInstance(_gameConfig.ScaleDownTime);

        BindInput();

        //FromNewComponentOn - creates GameManager as a new component on this gameobject
        //NonLazy - right after start of the scene
        Container.Bind<GameManager>().FromNewComponentOn(gameObject).AsSingle().NonLazy();
    }
    void BindInput()
    {
        //Declare of a Signal
        Container.DeclareSignal<ClickOnPlaneSignal>();
        Container.Bind<InputController>().FromNewComponentOn(gameObject).AsSingle().NonLazy();
    }
    void BindCharacters()
    {
        Container.Bind<CharacterFactory.Settings>().FromInstance(_gameConfig.CharacterFactorySettings).AsSingle();
        //FromFactory - in all CharacterActor.Factory injects CharacterFactory implementation
        Container.BindFactory<CharacterType, CharacterActor, CharacterActor.Factory>().FromFactory<CharacterFactory>();
        //Bind - in all CharacterPool injects CharacterPool instance
        Container.Bind<CharacterPool>().AsSingle();
        Container.Bind<CharacterSpawner>().FromNewComponentOn(gameObject).AsSingle();
    }
}