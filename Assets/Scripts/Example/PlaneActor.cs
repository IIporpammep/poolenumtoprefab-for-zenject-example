﻿using UnityEngine;
/// <summary>
/// Plane store _characterType so GameManager will know what CharacterActor to spawn on this plane
/// </summary>
public class PlaneActor : MonoBehaviour
{
    [SerializeField]
    CharacterType _characterType;


    public CharacterType GetCharacterType()
    {
        return _characterType;
    }
}
