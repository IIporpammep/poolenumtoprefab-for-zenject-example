﻿using UnityEngine;
using Zenject;
/// <summary>
/// Fired when a player clicked on any plane
/// </summary>
public class ClickOnPlaneSignal : Signal<ClickOnPlaneSignal, Vector3, CharacterType> { }
