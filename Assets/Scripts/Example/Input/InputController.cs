﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
/// <summary>
/// On a player mouse click raytrace from the MainCamera, if any plane hit fires ClickOnPlaneSignal
/// </summary>
public class InputController : MonoBehaviour
{
    ClickOnPlaneSignal _clickOnPlaneSignal;


    [Inject]
    void Construct(ClickOnPlaneSignal clickOnPlaneSignal)
    {
        _clickOnPlaneSignal = clickOnPlaneSignal;
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaytraceFromCamera(Input.mousePosition);
        }
    }
    void RaytraceFromCamera(Vector3 position)
    {
        Ray ray = Camera.main.ScreenPointToRay(position);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, 100f, 1 << LayerMask.NameToLayer("Plane")))
        {
            PlaneActor hitplane = hitInfo.collider.GetComponent<PlaneActor>();
            if (hitplane != null)
            {
                _clickOnPlaneSignal.Fire(hitInfo.point, hitplane.GetCharacterType());
            }
        }
    }
}
